import axiosInstance from "@/api/httpClient";

export default {
  async fetchCelebDetails({ commit }, celebId) {
    try {
      const response = await axiosInstance.get(`/personnel/actors-directors-details/${celebId}/`);
      commit('setCelebDetails', response.data);
    } catch (error) {
      console.error('Error fetching celeb details:', error);
      throw error; 
    }
  },

  async fetchAllDirectors({ commit }) {
    try{
      const response = await axiosInstance.get('/personnel/actors-directors/?role=Director');
      commit('setDirectors', response.data);
    } catch (error){
      console.log("Error fetching all directors: ", error);
      throw error;
    }
  },
  async fetchAllActors({ commit }) {
    try{
      let all_data = [];
      const response = await axiosInstance.get('/personnel/actors-directors/?role=Actor');
      all_data.push(response.data);
      commit('setActors', all_data);
    } catch (error){
      console.log("Error fetching all actors: ", error);
      throw error;
    }
  },
  async fetchAllActresses({ commit }) {
    try{
      let all_data = [];
      const responseActress = await axiosInstance.get('/personnel/actors-directors/?role=Actress');
      all_data.push(responseActress.data);
      commit('setActresses', all_data);
    } catch (error){
      console.log("Error fetching all actresses: ", error);
      throw error;
    }
  },

};

