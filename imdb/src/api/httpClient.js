import axios from 'axios';

const baseURL = 'http://127.0.0.1:8000';

const axiosInstance = axios.create({
    baseURL,
    timeout: 30000,
    // withCredentials: true, 
    headers: {
      "Content-Type": 'application/json'
    }
  });

export default axiosInstance;
