export default {
    postRating(context, payload) {
        const newRating = {
            id: new Date().toISOString(),
            // watchlistId: payload.watchlistId,
            rating: payload.rating,       
        };
        context.commit('addRating', newRating);
    }
}