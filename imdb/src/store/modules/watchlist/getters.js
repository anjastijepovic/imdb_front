export default {
    all_watchlist(state){
        return state.all_watchlist;
    },
    all_movies(state){
        return state.all_movies;
    },
    all_series(state){
        return state.all_series;
    },
    popular_movies(state){
        return state.popular_movies;
    },
    popular_series(state){
        return state.popular_series;
    },
    recent_movies(state){
        return state.recent_movies;
    },
    recent_series(state){
        return state.recent_series;
    },

    filtered_popular_movies(state) {
        return state.filtered_popular_movies;
    },
    filtered_popular_series(state) {
        return state.filtered_popular_series;
    },
    filtered_recent_movies(state) {
        return state.filtered_recent_movies;
    },
    filtered_recent_series(state) {
        return state.filtered_recent_series;
    },

    all_genres(state) {
        return state.all_genres;
    },

    createdSuccessfully(state) {
        return state.createdSuccessfully;
    },
};
