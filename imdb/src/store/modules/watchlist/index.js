import mutations from './mutations.js';
import getters from './getters.js';
import actions from './actions.js';

export default {
    namespaced: true,
    state() {
        return {
            all_watchlist: [],
            
            all_movies:[],
            all_series:[],
            
            popular_movies: [],
            popular_series: [],
            
            recent_movies: [],
            recent_series: [],

            filtered_popular_movies: [],
            filtered_popular_series: [],

            filtered_recent_movies: [],
            filtered_recent_series: [],

            all_genres: [],

            createdSuccessfully: true,

        };
    },
    mutations,
    actions,
    getters,
}