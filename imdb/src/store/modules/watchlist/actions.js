import axiosInstance from "@/api/httpClient";

export default {
    async moviesCreate(context, data){
        let selected_persons = [];
        selected_persons.push({"id": data.director});
        for(let actor_id of data.actors){
            selected_persons.push({"id": actor_id});
        }

        let selected_genres = [];
        for(let genre_id of data.genres){
            selected_genres.push({"id": genre_id});
        }
        const movieData = {
            title: data.title,
            description: data.desc,
            created: data.year,
            genres: selected_genres,
            persons: selected_persons,
        };
        const image = data.image;
        console.log("slika: "+ image);

        const token = context.rootGetters.token;
        // http post request
        try{
            const response = await axiosInstance.post("/watchlist/movies/all/", movieData, 
                
                {headers : {
                    Authorization: "Token " + token,
                    // 'Content-Type': 'multipart/form-data'
                }},
            );

            try{
                const new_movie_id = response.data.id;

                await axiosInstance.put(`/watchlist/movies/${new_movie_id}/`, image,
                    {headers : {
                        Authorization: "Token " + token,
                        'Content-Type': 'multipart/form-data'
                    }},
                );

                context.commit('addMovie', movieData);
            } catch(error) {
                console.log("error adding image: " + error);
            }

        } catch(error){
            console.log(error);
            // context.commit('setCreatedSuccessfully', {success: false});
            // console.log('You have to be logged in as admin to add a movie');
        }


        
    },
    async seriesCreate(context, data){
        let selected_persons = [];
        selected_persons.push({"id": data.director});
        for(let actor_id of data.actors){
            selected_persons.push({"id": actor_id});
        }

        let selected_genres = [];
        for(let genre_id of data.genres){
            selected_genres.push({"id": genre_id});
        }
        const seriesData = {
            title: data.title,
            description: data.desc,
            created: data.year ,
            genres: selected_genres,
            persons: selected_persons,
        };
        const image = data.image;

        const token = context.rootGetters.token;
        // http post request
        try{
            const response = await axiosInstance.post("/watchlist/series/all/",
                seriesData,
                {headers : {
                    Authorization: "Token " + token
                }}
            );

            try{
                const new_series_id = response.data.id;

                await axiosInstance.put(`/watchlist/series/${new_series_id}/`, image,
                    {headers : {
                        Authorization: "Token " + token,
                        'Content-Type': 'multipart/form-data'
                    }},
                );

                context.commit('addSeries', seriesData);
            } catch(error) {
                console.log("error adding image: " + error);
            }

        } catch(error){
            console.log(error);
            // console.log('You have to be logged in as admin to add series');
        }
    },

    // loadAllWatchlist(context, payload) {

    // },

    async loadAllMovies(context) {
        const { data } = await axiosInstance.get("/watchlist/movies/all/");
        context.commit('setAllMovies', data);
    },
    async loadAllSeries(context) {
        const { data } = await axiosInstance.get("/watchlist/series/all/");
        context.commit('setAllSeries', data);
    },

    async loadPopularMovies(context) {
        const { data } = await axiosInstance.get("/watchlist/movies/popular/");
        context.commit('setPopularMovies', data);
    },
    async loadPopularSeries(context) {
        const { data } = await axiosInstance.get("/watchlist/series/popular/");
        context.commit('setPopularSeries', data);
    },

    async loadRecentMovies(context) {
        const { data } = await axiosInstance.get("/watchlist/movies/recent/");
        context.commit('setRecentMovies', data);
    },
    async loadRecentSeries(context) {
        const { data } = await axiosInstance.get("/watchlist/series/recent/");
        context.commit('setRecentSeries', data);
    },

    async loadAllGenres(context) {
        const { data } = await axiosInstance.get("/watchlist/genres/all/");
        context.commit('setGenres', data);
    },
    
    loadFilteredPopularMovies(context, data) {
        context.commit('setFilteredPopularMovies', data);
    },
    loadFilteredPopularSeries(context, data) {
        context.commit('setFilteredRecentSeries', data);
    },
    loadFilteredRecentMovies(context, data) {
        context.commit('setFilteredRecentMovies', data);
    },
    loadFilteredRecentSeries(context, data){
        context.commit('setFilteredRecentSeries', data);
    },

    isCreatedSuccessfully(context, data) {
        context.commit('setCreatedSuccessfully', data);
    },

};
