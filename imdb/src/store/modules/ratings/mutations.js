export default {
    addRating(state, payload) {
      state.ratings.push(payload);
    },
    setRatings(state, payload) {
      state.ratings = payload;
    }
  };