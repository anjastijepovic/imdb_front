import { createStore } from "vuex";

import watchlistModule from './modules/watchlist/index.js';
import authModule from './modules/auth/index.js';

import celebsModule from './modules/people/index.js';
// import commentsModule from './modules/comments/index.js';
import ratingsModule from './modules/ratings/index.js';

const store = createStore({
    modules: {
        watchlist: watchlistModule,
        auth: authModule,

        celebs: celebsModule,
        // comments: commentsModule,
        ratings: ratingsModule,
    },
    // state() {
    //     return {
    //         username: 'filipj',
    //     };
    // },
    // getters: {
    //     user_username(state) {
    //         return state.username;
    //     }
    // }
});

export default store;

// --------------------------



// import authModule from './modules/auth/index.js';



