import axiosInstance from "@/api/httpClient";

export default {
    async login(context, payload){
        try{
            const { data } = await axiosInstance.post("/account/login/",
                {
                    username: payload.username,
                    password: payload.password,
                },
            );

            context.commit('setUser', {
                token: data.token,
                username: payload.username,
            })
            console.log(data);
            localStorage.setItem('token', data.token);
            localStorage.setItem('username', payload.username);

            context.commit('setLoginSuccess', {success: true});
        } catch(error){
            console.log(error);
            context.commit('setLoginSuccess', {success: false});
        }
    },
    async signup(context, payload){
        try{
            const { data } = await axiosInstance.post("/account/register/",
                {
                    username: payload.username,
                    password: payload.password,
                    email: payload.email,
                    password2: payload.password
                },
            );

            context.commit('setUser', {
                token: data.token,
                username: payload.username,
            })

            localStorage.setItem('token', data.token);
            localStorage.setItem('username', payload.username);

            // console.log("Signed up successfully");
            context.commit('setLoginSuccess', {success: true});
        } catch(error){
            console.log(error);
            context.commit('setLoginSuccess', {success: false});
        }
    },

    tryLogin(context){
        const token = localStorage.getItem('token');
        const username = localStorage.getItem('username');

        if (token && username) {
            context.commit('setUser', {
                token: token,
                username: username,
            });
        }
    },

    logout(context) {
        localStorage.removeItem('token');
        localStorage.removeItem('username');

        context.commit('setUser', {
            token: null,
            username: null,
        });
    },

    loginSuccessAction(context, payload) {
        context.commit('setLoginSuccess', payload);
    },

    beginningAction(context){
        context.commit('setBeginning', {value: false});
    },
};