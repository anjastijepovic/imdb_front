export default {
    setCelebDetails(state, celebDetails) {
      state.celebDetails = celebDetails;
    },
    setDirectors(state, directors) {
      state.allDirectors = directors;
    },
    setActors(state, actors) {
      state.allActors = actors;
    },
    setActresses(state, actresses) {
      state.allActresses = actresses;
    }
  };