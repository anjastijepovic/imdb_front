export default {
    ratings(state) {
      return state.ratings;
    }, 
    hasRatings(state) {
      return state.ratings && state.ratings.length > 0;
    }
  };
