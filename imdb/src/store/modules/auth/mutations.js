export default {
    setUser(state, payload){
        state.token = payload.token
        state.username = payload.username
    },
    setLoginSuccess(state, payload){
        state.loginSuccess = payload.success;
    },
    setBeginning(state, payload){
        state.beginning = payload.value;
    },
};