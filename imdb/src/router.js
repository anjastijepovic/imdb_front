import { createRouter, createWebHistory } from "vue-router";

import HomePage from "./pages/watchlist/HomePage.vue";
import WatchlistDetail from "./pages/watchlist/WatchlistDetail.vue";
import WatchlistCreate from "./pages/watchlist/WatchlistCreate.vue";
import EpisodeDetail from "./pages/watchlist/EpisodeDetail.vue";
import FavoritesPage from "./pages/watchlist/FavoritesPage.vue";

import UserAuth from "./pages/auth/UserAuth.vue";
import NotFound from "./pages/NotFound.vue";

import store from "./store/index.js";

import CelebDetail from './pages/people/CelebDetail.vue';
// import CommentRatingList from './components/reviews/CommentRatingList.vue';


const router = createRouter({
  history: createWebHistory(),
  routes: [
    { path: "/", redirect: "/homepage" },
    { path: "/homepage", component: HomePage },
    { path: "/watchlist/:id", props: true, component: WatchlistDetail },

    // { 
    //   path: '/watchlist/:type/:id', 
    //   name: 'MovieDetail', 
    //   component: WatchlistDetail, 
    //   props: true, 
    //   // meta: { type: 'movie' }
    // },
    // { 
    //   path: '/watchlist/:type/:id', 
    //   name: 'SerieDetail', 
    //   component: WatchlistDetail, 
    //   props: true, 
    //   // meta: { type: 'series' }
    // },
    
    { path: "/watchlist-create", component: WatchlistCreate, meta: { requiresAuth: true } },
    { path: "/episode/:id", props: true, component: EpisodeDetail },
    { path: "/favorites", component: FavoritesPage, meta: { requiresAuth: true } },

    { path: "/signin", component: UserAuth, meta: { requiresUnauth: true } },

    { path: "/:notFound(.*)", component: NotFound },

    //---------------------------------------------------------------------------

    {
      path: '/celebs/:id',
      props: true,
      component: CelebDetail,
    },
    // {
    //   path: '/review-list/',
    //   props: true,
    //   component: CommentRatingList
    // },
  ],
});

router.beforeEach((to, from, next) => {
  if(to.meta.requiresAuth && !store.getters.isAuthenticated){
    next('/signin');
  } else if(to.meta.requiresUnauth && store.getters.isAuthenticated){
    next('/homepage');
  } else {
    next();
  }
});


export default router;



