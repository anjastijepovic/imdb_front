import mutations from "./mutations.js";
import actions from "./actions.js";
import getters from "./getters.js";

export default {
    state() {
        return {
            // userId: null,
            username: null,
            token: null,
            loginSuccess: true,
            beginning: true,
        };
    },
    mutations,
    actions,
    getters,
}