export default {
    celebDetails(state) {
      return state.celebDetails;
    },
    hasCelebDetails(state) {
      return state.celebs && state.celebs.length > 0;
      // return state.celebs && Object.keys(state.celebs).length > 0;
    },
    // ispravi hasCelebDetails

    allDirectors(state) {
      return state.allDirectors;
    },
    allActors(state) {
      return state.allActors;
    },
    allActresses(state) {
      return state.allActresses;
    }
  };
  