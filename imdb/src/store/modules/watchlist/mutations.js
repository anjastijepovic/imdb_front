export default {
    addMovie(state, payload){
        state.all_movies.push(payload);
        state.all_watchlist.push(payload);
        state.recent_movies.push(payload);
    },
    addSeries(state, payload){
        state.all_series.push(payload);
        state.all_watchlist.push(payload);
        state.recent_series.push(payload);
    },

    setAllWatchlist(state, payload){
        state.all_watchlist = payload;
    },

    setAllMovies(state, payload){
        state.all_movies = payload;
    },
    setAllSeries(state, payload){
        state.all_series = payload;
    },

    setPopularMovies(state, payload){
        // state.popular_movies.push(1);
        // state.popular_movies.pop();
        state.popular_movies = [...payload];
    },
    setPopularSeries(state, payload){
        state.popular_series = payload;
    },

    setRecentMovies(state, payload){
        state.recent_movies = payload;
    },
    setRecentSeries(state, payload){
        state.recent_series = payload;
    },

    setFilteredPopularMovies(state, payload){
        state.filtered_popular_movies = payload;   
    },
    setFilteredPopularSeries(state, payload){
        state.filtered_popular_series = payload;   
    },
    setFilteredRecentMovies(state, payload){
        state.filtered_recent_movies = payload;   
    },
    setFilteredRecentSeries(state, payload){
        state.filtered_recent_series = payload;   
    },

    setGenres(state, payload) {
        state.all_genres = payload;
    },

    setCreatedSuccessfully(state, payload) {
        state.createdSuccessfully = payload.success;
    },

};